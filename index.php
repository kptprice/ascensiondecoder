<!DOCTYPE html>
<html>
<head>
    <title>Ascension Code Decoder</title>
</head>
<body>
<div class="container">
    <form class="form-inline" action="decoder.php" method="post">
        <textarea name="message" class="form-control" rows="5" placeholder="Type your message here"></textarea>
        <textarea name="key" class="form-control" rows="5" placeholder="Type your key here"></textarea>
        <button type="submit" class="btn btn-default">Generate Echelon Code</button>
    </form>
</div>
</body>
</html>